# -*- mode: snippet -*-
# name: Start, stop and wipe a PostgreSQL engine
# key: pgssw
# expand-env: ((yas-indent-line 'fixed))
# --
##############
# PostgreSQL #
##############

# Start the PostgreSQL engine
start:
  #!/usr/bin/env bash
  set -euo pipefail

  if [[ -n "$PGDATA" ]]
  then
    if [[ -e "$PGDATA/postmaster.pid" ]] && ! pg_ctl status > /dev/null 2>&1
    then
      echo "Removing leftover '$PGDATA/postmaster.pid'"
      rm "$PGDATA/postmaster.pid"
    fi
    if [[ ! -e "$PGDATA/postmaster.pid" ]]
    then
      sockets_dir=$(mktemp --directory --tmpdir pg_sockets.XXXXX)
      if [[ -e "$PGDATA/postgresql.conf" ]]
      then
        sed -i "s:unix_socket_directories = '.*':unix_socket_directories = '$sockets_dir':" "$PGDATA/postgresql.conf"
      else
        initdb --auth-host trust --encoding UTF-8 --username "$PGUSER" --pwfile <(echo $PGPASSWORD)
        echo -e "\nunix_socket_directories = '$sockets_dir'" >> "$PGDATA/postgresql.conf"
      fi
      echo -n "Starting PostgreSQL server: "
      mkdir -p "$LOGS_DIR"
      pg_ctl --log "$LOGS_DIR/postgresql.log" start
    fi
  fi
  if [[ -n "$PGDATABASE" ]]
  then
    if psql -l --csv | grep --quiet "^$PGDATABASE,"
    then
      echo "Default '$PGDATABASE' database already exists"
    else
      createdb "$PGDATABASE"
      echo "Created default '$PGDATABASE' database"
    fi
  fi

# Stop the PostgreSQL engine
stop:
  #!/usr/bin/env bash
  set -euo pipefail

  if [[ -n "$PGDATA" ]]
  then
    echo -n "Stopping PostgreSQL server: "
    pg_ctl stop
  fi

# Remove PostgreSQL data
[confirm('Do you really want to wipe all existing databases?')]
wipe: stop
  #!/usr/bin/env bash
  set -euo pipefail

  if [[ -n "$PGDATA" ]]
  then
    rm -rf "$LOGS_DIR/postgresql.log"
    if [[ -e "$PGDATA" ]]
    then
      rm -rf "$PGDATA"
    fi
    echo "PostgreSQL engine wiped"
  fi
