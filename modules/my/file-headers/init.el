;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — File headers initialization
;; :Created:   ven 10 dic 2021, 13:17:04
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

;; The project name, license and copyright holder can be easily customized within a
;; .dir-locals.el file, for example:
;;
;;  ((nil . ((my+fh/project-name . "my-project")
;;           (my+fh/project-license . "MIT License")
;;           (my+fh/project-copyright-holder . "Arstecnica s.r.l."))))
;;

;; Backward compatibility with my old emacs-starter-kit

(defvaralias 'esk/project-name 'my+fh/project-name)
(put 'esk/project-name 'safe-local-variable 'stringp)

(defvaralias 'esk/project-license 'my+fh/project-license)
(put 'esk/project-license 'safe-local-variable 'stringp)

(defvaralias 'esk/project-copyright-holder 'my+fh/project-copyright-holder)
(put 'esk/project-copyright-holder 'safe-local-variable 'stringp)

(defvaralias 'esk/user-full-name 'my+fh/user-full-name)
(put 'esk/user-full-name 'safe-local-variable 'stringp)

(defvaralias 'esk/user-mail-address 'my+fh/user-full-name)
(put 'esk/user-mail-address 'safe-local-variable 'stringp)

(defvar my+fh/project-name nil
  "Last project name.")

(put 'my+fh/project-name 'safe-local-variable 'stringp)

(defun my+fh/project-name ()
  (setq my+fh/project-name (read-string "Project: " my+fh/project-name)))

(defvar my+fh/project-license "GNU General Public License version 3 or later"
  "Last project license.")

(put 'my+fh/project-license 'safe-local-variable 'stringp)

(defun my+fh/project-license ()
  (setq my+fh/project-license (read-string "License: " my+fh/project-license)))

(defvar my+fh/project-copyright-holder user-full-name
  "Last project copyright holder")

(put 'my+fh/project-copyright-holder 'safe-local-variable 'stringp)

(defun my+fh/project-copyright-holder ()
  (setq my+fh/project-copyright-holder
        (read-string "Copyright holder: " my+fh/project-copyright-holder)))

(defvar my+fh/user-full-name nil
  "Full name of the author.")

(put 'my+fh/user-full-name 'safe-local-variable 'stringp)

(defun my+fh/user-full-name ()
  (setq my+fh/user-full-name
        (read-string "Author name: " (or my+fh/user-full-name user-full-name))))

(defvar my+fh/user-mail-address nil
  "Email address of the author.")

(put 'my+fh/user-mail-address 'safe-local-variable 'stringp)

(defun my+fh/user-mail-address ()
  (setq my+fh/user-mail-address
        (read-string "Author email: " (or my+fh/user-mail-address user-mail-address))))
