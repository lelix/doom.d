;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Required packages
;; :Created:   mer 14 ago 2023, 10:28:29
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2024 Lele Gaifax
;;

(package! d2-mode)
