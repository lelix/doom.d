;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Configuration
;; :Created:   mer 14 ago 2023, 10:27:10
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2024 Lele Gaifax
;;

(use-package! d2-mode
  :mode "\\.d2\\'")
