# -*- coding: utf-8 -*-
# :Project:   doom.d — My bindings
# :Created:   ven 10 dic 2021, 13:09:00
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2021, 2022, 2023, 2024 Lele Gaifax
#

* Table of Contents :TOC_3:
- [[#description][Description]]
  - [[#module-flags][Module Flags]]
- [[#plugins][Plugins]]
- [[#bindings][Bindings]]
  - [[#function-keys][Function keys]]
  - [[#window-selection][Window selection]]
  - [[#switch-buffers][Switch buffers]]
  - [[#rename-delete-or-copy-current-buffer-file-name][Rename, delete or copy current buffer file name]]
  - [[#drag-stuff][Drag stuff]]
  - [[#surrounding-symbols-with-quotes][Surrounding symbols with quotes]]
  - [[#python][Python]]
  - [[#remove-everything-up-to-given-char][Remove everything up to given char]]
  - [[#extra-toggles][Extra toggles]]
  - [[#font-size][Font size]]
  - [[#copy-link-to-upstream-git-repository][Copy link to upstream Git repository]]

* Description

This modules recreates an environment where my fingers feel at home, bringing in
the key bindings from the previous [[https://github.com/lelit/emacs-starter-kit][ESK]].

** Module Flags
+ [[file:+fkeys.el][+fkeys]] :: Activate function keys
+ ~+windmove~ :: Activate default ~windmove~ bindings

* Plugins

+ [[https://github.com/sshaw/git-link][git-link]]
+ [[https://github.com/atykhonov/google-translate][google-translate]]
+ [[https://github.com/victorhge/iedit][iedit]]

* Bindings

These are the bindings configured by this module.

** Function keys

*Only* when the ~+fkeys~ option is enabled.

| Key             | Command                              | Description                                                    |
|-----------------+--------------------------------------+----------------------------------------------------------------|
| =F1=            | hippie-expand                        | expand text before point using several different methods       |
| =F2=            | query-replace                        | replace literal string with something else                     |
| =Ctrl-F2=       | map-query-replace-regexp             | replace regexp with a series of strings                        |
| =Shift-F2=      | query-replace-regexp                 | replace regexp with something else                             |
| =Meta-F2=       | iedit-mode                           | interactively replace all occurrences with something else      |
|                 |                                      |                                                                |
| =F3=            | grep                                 | run ~grep~ prompting for arguments                             |
| =Ctrl-F3=       | consult-ripgrep                      | run ~rg~ and display live search result                        |
| =Shift-F3=      | grep-find                            | run ~grep~ driven by ~find~                                    |
| =Meta-F3=       | my+git/grep                          | run ~git grep~                                                 |
| =Shift-Meta-F3= | consult-git-grep                     | run ~git grep~ and display live search result                  |
|                 |                                      |                                                                |
| =F4=            | kmacro-end-or-call-macro             | if recording a macro end it, otherwise execute it              |
| =Ctrl-F4=       | kmacro-keymap                        | a rich set of kmacro functions (reachable also with =C-x C-k=) |
| =Shift-F4=      | kmacro-start-macro-or-insert-counter | create a new macro, or insert a counter if already recording   |
| =Meta-F4=       | kmacro-edit-macro-repeat             | edit last recorded macro                                       |
|                 |                                      |                                                                |
| =F5=            | my+compile/next-makefile             | run ~make~ in the closer directory containing a =Makefile=     |
|                 |                                      |                                                                |
| =F6=            | next-error                           | goto next compilation error/match                              |
| =Shift-F6=      | previous-error                       | goto previous error/match                                      |
|                 |                                      |                                                                |
| =F7=            | flycheck-next-error                  | goto next ~flycheck~ error                                     |
| =Shift-F7=      | flycheck-previous-error              | goto previous ~flycheck~ error                                 |
|                 |                                      |                                                                |
| =F8=            | my+ispell/cycle-languages            | change the dictionary used by spelling checker                 |
| =Shift-F8=      | my/google-translate-at-point         | translate the word at point                                    |
| =Ctrl-F8=       | my/google-translate-query            | ask for the text to translate                                  |
|                 |                                      |                                                                |
| =F9=            | magit-status                         | show the ~magit~ status buffer                                 |
| =Shift-F9=      | magit-blame-addition                 | show ~git blame~ of current file                               |
| =Meta-F9=       | magit-log-buffer-file                | show ~git log~ of current file                                 |
|                 |                                      |                                                                |
| =F10=           | shell                                | run a interactive shell                                        |
| =Shift-F10=     | eshell                               | run an /elisp/ shell                                           |
| =Meta-F10=      | term                                 | run a terminal                                                 |
|                 |                                      |                                                                |
| =F11=           | pyvenv-activate                      | activate a Python virtual environment                          |
|                 |                                      |                                                                |
| =F12=           |                                      | shorter alias of =C-c t=, a rich set of toggling functions     |

** Window selection

| Key           | Command        | Description                                    |
|---------------+----------------+------------------------------------------------|
| =Shift-UP=    | windmove-up    | move the point to the window above current one |
| =Shift-DOWN=  | windmove-down  | move the point to the window below             |
| =Shift-LEFT=  | windmove-left  | move the point to the window on the left       |
| =Shift-RIGHT= | windmove-right | move the point to the window on the right      |

** Switch buffers

| Key           | Command         | Description                       |
|---------------+-----------------+-----------------------------------|
| =Ctrl-PGUP=   | previous-buffer | switch to the previous buffer     |
| =Ctrl-PGDOWN= | next-buffer     | switch to the next buffer         |

The following are equivalent and comes from my old ~ESK~: they are /deprecated/
because for example ~Org~ redefines them to completely different things. I'm
slowly retraining myself to the above, and will eventually remove these.

| Key          | Command         | Description                   |
|--------------+-----------------+-------------------------------|
| =Meta-LEFT=  | previous-buffer | switch to the previous buffer |
| =Meta-RIGHT= | next-buffer     | switch to the next buffer     |

** Rename, delete or copy current buffer file name

| Key        | Command                          | Description                                   |
|------------+----------------------------------+-----------------------------------------------|
| =Ctrl-c R= | my/rename-current-buffer-file    | change the name of the file in current buffer |
| =Ctrl-c D= | my/delete-current-buffer-file    | delete the file in current buffer and kill it |
| =Ctrl-c W= | my/copy-current-buffer-file-path | copy the full path of the current buffer file |

** Drag stuff

These operate on the current region.

| Key                | Command          | Description                              |
|--------------------+------------------+------------------------------------------|
| =Meta-Shift-UP=    | drag-stuff-up    | drag current line/selection up           |
| =Meta-Shift-DOWN=  | drag-stuff-down  | drag current line/selection down         |
| =Meta-Shift-LEFT=  | drag-stuff-left  | drag current word/selection to the left  |
| =Meta-Shift-RIGHT= | drag-stuff-right | drag current word/selection to the right |

** Surrounding symbols with quotes

These operate on the current region.

| Key      | Command                         | Description                                  |
|----------+---------------------------------+----------------------------------------------|
| =Ctrl-'= | my/single-quote-symbol          | wrap symbol at point between single quotes   |
| =Meta-'= | my/single-quote-previous-symbol | wrap preceeding symbol between single quotes |
| =Ctrl-"= | my/double-quote-symbol          | wrap symbol at point between double quotes   |
| =Meta-"= | my/double-quote-previous-symbol | wrap preceeding symbol between single quotes |

** Python

These are installed when ~:lang python~ is enabled.

| Key           | Command                | Description                                   |
|---------------+------------------------+-----------------------------------------------|
| =Ctrl-RETURN= | my+python/split-string | split current string in two consecutive lines |

** Remove everything up to given char

| Key            | Command        | Description                                  |
|----------------+----------------+----------------------------------------------|
| =Meta-Shift-z= | zap-up-to-char | kill up to, but not including, a given char. |

On a [[help:display-graphic-p][graphic display]] the same function is bound also to =Ctrl-z=.

** Extra toggles

The following are also available on the =F12= key.

| Key         | Command Function         | Description                                       |
|-------------+--------------------------+---------------------------------------------------|
| =C-c t m=   | menu-bar-mode            | toggle visibility of the menu bar                 |
| =C-c t S=   | my/toggle-window-split   | switch between horizontal/vertical windows layout |
| =C-c t T=   | my/switch-to-other-theme | switch between current theme and `my/other-theme' |
| =C-c t C-f= | follow-mode              | toggle continuous display in multiple windows     |
| =C-c t C-s= | scroll-all-mode          | attempt to synchronize scroll in visible windows  |

** Font size

In addition to the =C-c t b= (~doom-big-font-mode~) toggle, the following global
bindings change the font size.

| Key     | Command Function        | Description                                                  |
|---------+-------------------------+--------------------------------------------------------------|
| =C-/=   | doom/reset-font-size    | Reset font size and text-scale                               |
| =C-+=   | doom/increase-font-size | Enlarge the font size across the current and child frames    |
| =C--=   | doom/decrease-font-size | Shrink the font size across the current and child frames     |
| =C-M-+= | text-scale-increase     | Increase the font size of the default face in current buffer |
| =C-M--= | text-scale-decrease     | Decrease the font size of the default face in current buffer |

** Copy link to upstream Git repository

| Key       | Command  | Description                           |
|-----------+----------+---------------------------------------|
| =C-c c h= | git-link | Copy link to upstream Git repository. |
