;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Required packages
;; :Created:   mar 14 dic 2021, 11:02:15
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022, 2024 Lele Gaifax
;;

(package! git-link
  :pin "aded95807f277f30e1607313bdf9ac9a016a2305")

(package! google-translate
  :pin "e60dd6eeb9cdb931d9d8bfbefc29a48ef9a21bd9")

(package! iedit
  :pin "27c61866b1b9b8d77629ac702e5f48e67dfe0d3b")
