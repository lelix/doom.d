;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Git helpers
;; :Created:   ven 10 dic 2021, 13:11:38
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(autoload 'magit-toplevel "magit-git")
(autoload 'magit-process-git "magit-process")

(defun my+git/outer-level (&optional dir)
  "Return the outer repository toplevel directory, or nil if there isn't one."
  (unless dir
    (setq dir (magit-toplevel)))
  (magit-toplevel (file-name-directory (directory-file-name dir))))

(defun my+git/toplevel (&optional outmost)
  "Return the toplevel directory of current repository.
When OUTMOST is non-nil, assume this is a nested repository (a
submodule for example) and return the directory containing the
outmost repository instead."
  (let ((toplevel (magit-toplevel))
        outer)
    (when outmost
      (while (setq outer (my+git/outer-level toplevel))
        (setq toplevel outer)))
    toplevel))

;;;###autoload
(defun my+git/grep (command)
  "Run `git grep' on the word at point or the words in the active region.

By default the search is executed only in the current Git repository,
starting from its top level directory.

With an universal prefix argument it recurses down also in all submodules.

With two univeral prefix arguments, the search starts in the outmost Git
repository recursing down in all submodules.

With an universal prefix argument equal to 0 the search starts from
the current directory."
  (interactive
   (let ((what (if (use-region-p)
                   (buffer-substring-no-properties (region-beginning) (region-end))
                 (thing-at-point 'symbol t)))
         (toplevel (if (= (prefix-numeric-value current-prefix-arg) 0)
                       default-directory
                     (my+git/toplevel (= (prefix-numeric-value current-prefix-arg) 16))))
         (git-grep "git --no-pager grep -n --color=always ")
         grep-command)
     (setq what (shell-quote-argument
                 (read-from-minibuffer "Regexp to search: " what)))
     (if (and current-prefix-arg (> (prefix-numeric-value current-prefix-arg) 0))
         (let ((git-submodule-foreach "git --no-pager submodule --quiet foreach --recursive ")
               (sed (concat "sed \"s,^,$toplevel/$path/,;s,^"
                            (expand-file-name toplevel) ",,\"")))
           (setq grep-command (concat "(" git-grep what " || :) && "
                                      git-submodule-foreach
                                      "'(" git-grep what " | " sed ") || :'")))
       (setq grep-command (concat git-grep what)))
     (list (read-shell-command
            "Run: "
            (concat
             "cd " toplevel " && " grep-command
             (if my+git/grep-exclusions
                 (concat " . "
                         (mapconcat
                          (lambda (e) (concat ":!" (shell-quote-argument e)))
                          my+git/grep-exclusions
                          " "))))))))
  (compilation-start command 'grep-mode))
