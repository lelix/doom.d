;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Buffer helpers
;; :Created:   ven 24 giu 2022, 15:58:01
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2022 Lele Gaifax
;;

;;;###autoload
(defun my/rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name-default "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

;;;###autoload
(defun my/delete-current-buffer-file ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer))
        (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
        (ido-kill-buffer)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

;; Adapted from http://xahlee.info/emacs/emacs/emacs_copy_file_path.html

;;;###autoload
(defun my/copy-current-buffer-file-path (&optional dir-path-only-p)
  "Copy the current buffer's file path or dired path to `kill-ring'.
Result is full path.
If `universal-argument' is called first, copy only the dir path.

If in dired, copy the file/dir cursor is on, or marked files.

If a buffer is not file and not dired, copy value of `default-directory'
(which is usually the “current” dir when that buffer was created)"
  (interactive "P")
  (let ((fpath
         (if (string-equal major-mode 'dired-mode)
             (let ((result (mapconcat 'identity (dired-get-marked-files) "\n")))
               (if (equal (length result) 0)
                   default-directory
                 result))
           (if (buffer-file-name)
               (buffer-file-name)
             (expand-file-name default-directory)))))
    (kill-new
     (if dir-path-only-p
         (progn
           (message "Directory copied: %s" (file-name-directory fpath))
           (file-name-directory fpath))
       (message "File path copied: %s" fpath)
       fpath))))
