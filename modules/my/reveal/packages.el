;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Required packages
;; :Created:   sab 5 ott 2024, 12:40:23
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2024 Lele Gaifax
;;

(package! org-reveal)
