;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Configuration
;; :Created:   sab 5 ott 2024, 12:41:06
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2024 Lele Gaifax
;;

(use-package! ox-reveal
  :init
  (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js"))
