;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Required packages
;; :Created:   mer 29 mar 2023, 11:46:40
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2023, 2024 Lele Gaifax
;;

(package! org-jira
  :pin "295b01ede42952c848bd8d76bc8c456a87876cbc")

(package! ox-jira)
