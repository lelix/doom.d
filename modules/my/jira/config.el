;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Configuration
;; :Created:   mer 29 mar 2023, 11:48:49
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2023 Lele Gaifax
;;

(use-package! org-jira)

(use-package! ox-jira)

(with-eval-after-load 'jiralib
  (put 'jiralib-url 'safe-local-variable 'stringp))
