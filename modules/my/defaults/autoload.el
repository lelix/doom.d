;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Generic helpers
;; :Created:   dom 19 dic 2021, 18:56:28
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

;; Customize my main Emacs instance: I'm used to have one Emacs dedicated to
;; news, mail, chat and so on, living in the second i3 workspace. This function
;; is then called by my i3 configuration file with
;;
;;  exec --no-startup-id i3-msg 'workspace 2; exec emacs -f my/main-emacs-!; workspace 1'

;;;###autoload
(defun my/main-emacs (&optional dont-ask)
  "Connect to IRC, GNUS, Notmuch and activate Emacs server, but ask first.
If DONT-ASK is non-nil, interactively when invoked with a prefix arg,
start everything unconditionally."
  (interactive "P")

  (when (or dont-ask (y-or-n-p "Emacs server? "))
    (server-start))

  (when (and (modulep! :email notmuch)
             (or dont-ask (y-or-n-p "Notmuch? ")))
    (=notmuch))

  (when (and (modulep! :my gnus)
             (or dont-ask (y-or-n-p "GNUS? ")))
    (my/gnus))

  (when (and (modulep! :app irc)
             (or dont-ask (y-or-n-p "IRC? ")))
    (=irc))

  (when (and (modulep! :app rss)
             (or dont-ask (y-or-n-p "Elfeed? ")))
    (elfeed))

  (message "Have a nice day!"))

;;;###autoload
(defun my/main-emacs-! ()
  "Unconditionally start my emacs setup."
  ;; Give to Doom's deferred loading machinery the time to reach stability,
  ;; otherwise it breaks on not-yet-initialized vars in the persp-mode
  ;; namespace; this is not ideal, but works for now.
  (run-at-time 5 0 #'my/main-emacs t))
