;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — My own specific prefs
;; :Created:   sab 11 dic 2021, 10:00:27
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;; More reasonable line width limit, given modern wide monitors
(setq-default fill-column 95)

;; Disable automatic continuation of comments
(setq! +default-want-RET-continue-comments nil)

;; Ya, I know what's going to happen, thanks
(setq! confirm-kill-emacs nil)
