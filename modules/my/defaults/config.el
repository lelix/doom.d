;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Defaults configuration
;; :Created:   ven 10 dic 2021, 13:13:37
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

(when (modulep! +ui)
  (load! "+ui"))

(when (modulep! +modes)
  (load! "+modes"))

(when (modulep! +packages)
  (load! "+packages"))

(load! (concat "config-" user-login-name) nil t)
