;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Defaults packages
;; :Created:   ven 10 dic 2021, 13:14:03
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022, 2023, 2024 Lele Gaifax
;;

(package! subword :built-in t)
(package! whitespace :built-in t)

;; Disable JS tide, does not work for me...
(package! tide :disable t)

;; Disable savehist, I do not find any value in it
(package! savehist :disable t)

(package! po-mode
  :recipe `(:host github
            :repo "lelit/po-mode"
            :branch "my-hacks"
            :files ("po-mode.el"))
  :pin "788b5beba9fb4407df0bd9a9601adcd99d7d1e9b")

(package! pycov-mode
  :recipe `(:host github
            :repo "lelit/pycov-mode")
  :pin "5cbae4abbc7bc4b550814f92544fb1e6bf0f2443")

(package! page-break-lines
  :pin "e33426ae7f10c60253afe4850450902919fc87fd")

(package! magit-delta
  :pin "5fc7dbddcfacfe46d3fd876172ad02a9ab6ac616")

(package! lorem-ipsum
  :pin "4e87a899868e908a7a9e1812831d76c8d072f885")

(package! vbnet-mode
  :recipe `(:host github
            :repo "lelit/vbnet-mode")
  :pin "416db89933e15609603d43d526dc7c969c343715")

(package! just-mode
  :pin "57d854c26033c2f629b63fa4be90236fd3015278")

(package! cascading-dir-locals
  :pin "345d4b70e837d45ee84014684127e7399932d5e6")
