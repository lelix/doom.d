;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Defaults packages configuration
;; :Created:   ven 10 dic 2021, 13:15:20
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022, 2023, 2024 Lele Gaifax
;;

;; Enable subword mode in all programming modes
(use-package! subword
  :config
  (add-hook 'prog-mode-hook #'subword-mode))

;; Use nxml-mode for Zope's .zcml files
(use-package! nxml-mode
  :mode "\\.zcml\\'")

;; Use web-mode vbnet, framework7, Mako and Zope templates
(use-package! web-mode
  :mode "\\.\\(as[cp]x\\|f7\\|mako\\|pt\\)\\'"
  :custom
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-enable-auto-indentation nil))

;; Adjust default indentation in CSS
(use-package! css-mode
  :custom
  (css-indent-offset 2))

;; Use makefile-mode (gmake variant) for any Makefile.xxx
(use-package! makefile-gmake-mode
  :mode "Makefile\\.?")

;; Use po-mode for gettext catalogs
(use-package! po-mode
  :mode "\\.po\\'")

;; Enable pycov-mode
(use-package! pycov-mode
  :config
  (add-hook 'python-mode-hook #'pycov-mode)
  (map!
   :map python-mode-map
   :desc "Toggle coverage mode" "C-c t C" #'pycov-mode))

;; Display ^L page breaks as tidy horizontal lines
(use-package! page-break-lines
  :hook (after-init . global-page-break-lines-mode))

(when (and (modulep! :tools magit)
           (executable-find "delta"))
  (use-package! magit-delta
    :disabled
    :hook (magit-mode . magit-delta-mode)))

(use-package! lorem-ipsum
  :config
  (map!
   :leader
   :prefix "i"
   :desc "Insert lorem ipsum list" "l" #'lorem-ipsum-insert-list
   :desc "Insert lorem ipsum paragraphs" "p" #'lorem-ipsum-insert-paragraphs
   :desc "Insert lorem ipsum sentences" "s" #'lorem-ipsum-insert-sentences))

(use-package! vbnet-mode
  :mode "\\.vb\\'")

(use-package! just-mode)

(use-package! cascading-dir-locals
  :ensure
  :config
  (cascading-dir-locals-mode 1))
