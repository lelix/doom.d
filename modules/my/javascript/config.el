;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — JS configuration
;; :Created:   dom 26 dic 2021, 10:08:49
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(after! js2-mode
  (setq!
   js2-include-jslint-globals nil
   js2-indent-switch-body t
   js2-basic-offset 2)

  (add-hook 'js2-mode-hook #'js2-mode-display-warnings-and-errors)
  (add-hook 'js2-mode-hook #'js2-highlight-unused-variables-mode)
  (add-hook 'js2-mode-hook
            (lambda ()
              ;; Parse additional externs
              (add-hook 'js2-post-parse-callbacks
                        #'my+js2/apply-jsl-declares nil 'local)))

  (add-to-list 'global-mode-string
               '(js2-highlight-unused-variables-mode
                 ("%e" (:eval (my+js2/errors-and-warnings))))
               'append)

  (set-popup-rule! "^\\*js-lint\\*$"
    :side 'bottom :size 0.25 :select t :quit t))
