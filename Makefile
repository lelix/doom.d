# -*- coding: utf-8 -*-
# :Project:   doom.d — Simplify doom execution when DOOMDIR is not ~/.doom.d
# :Created:   sab 4 dic 2021, 8:22:48
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2021 Lele Gaifax
#

DOOM := DOOMDIR=$(CURDIR) DOOMLOCALDIR=$(CURDIR)/.local $(shell PATH=~/.emacs.d/bin:../bin:$(PATH) command -v doom)

sync:
	$(DOOM) sync

help:
	$(DOOM) --help

%:
	$(DOOM) $@
